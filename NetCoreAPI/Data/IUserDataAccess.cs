﻿using NetCoreAPI.Model;
using NetCoreAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreAPI.Data
{
    public interface IUserDataAccess
    {
        void SaveUser(UserViewModel userViewModel);
        UserModel AuthenticateUser(UserModel userModel);
        List<UserModel> GetAllUserList();

    }
}
