﻿using Microsoft.EntityFrameworkCore;
using NetCoreAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreAPI.Data
{
    public class UserDBContext:DbContext
    {
        public DbSet<UserViewModel> Identity { get; set; }

        public UserDBContext()
        {

        }
        public UserDBContext (DbContextOptions <UserDBContext> options): base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=SALMAN; Database=UserDB; Trusted_Connection=true;");
        }
    }
}
