﻿using Microsoft.IdentityModel.Tokens;
using NetCoreAPI.Model;
using NetCoreAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace NetCoreAPI.Data
{
    public class UserDataAccess:IUserDataAccess
    {

        public void SaveUser(UserViewModel userViewModel)
        {
            try
            {
                using (var context = new UserDBContext())
                {
                    context.Add<UserViewModel>(userViewModel);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                
            }
        }


        public UserModel AuthenticateUser(UserModel login)
        {
            UserModel user = null;

            try
            {
                using (var context = new UserDBContext())
                {
                    var data = context.Identity.Where(x => x.UserName.ToLower() == login.userName.ToLower() && x.Password == login.Password).FirstOrDefault();
                    if (data != null)
                    {
                        user = new UserModel { userName = data.UserName, Password = data.Password };
                    }
                }

                return user;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        public List<UserModel> GetAllUserList()
        {
            try
            {
                List < UserModel > userList = new List<UserModel>();
                using (var context = new UserDBContext())
                {
                    userList = context.Identity.Select(x=>new UserModel(){ userName = x.UserName, Password = x.Password}).ToList();
                   
                }

                return userList;
            }
            catch (Exception ex)
            {

                return null;
            }

        }



    }
}
