﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using NetCoreAPI.Data;
using NetCoreAPI.Model;
using NetCoreAPI.ViewModel;
using Newtonsoft.Json;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace NetCoreAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IConfiguration _config;
        private readonly IUserDataAccess _userDataAccess;

        public UserController(IConfiguration config, IUserDataAccess userDataAccess)
        {
            _config = config;
            _userDataAccess = userDataAccess;
        }

        [HttpPost]
        public IActionResult LoginUser([FromBody] UserModel userModel)
        {
            UserModel login = new UserModel();
            login.userName = userModel.userName;
            login.Password = userModel.Password;
            IActionResult response = Unauthorized();

            var user = _userDataAccess.AuthenticateUser(login);

            if(user != null)
            {
                var tokenStr = GenerateJSONWebToken(user);
                response = Ok(new {token=tokenStr });
            }

            return response;
        }
        

        [Authorize]
        [HttpGet]

        public ActionResult GetAllUser()
        {
            var data= _userDataAccess.GetAllUserList();
            var list = JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });

            return Content(list, "application/json");
        }


        [HttpPost]
        public ActionResult register([FromBody] UserViewModel userViewModel)
        {
            _userDataAccess.SaveUser(userViewModel);
            return Ok("Success");
        }




        private string GenerateJSONWebToken(UserModel userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub,userInfo.userName),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())

            };

            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials
                );

            var encodetoken = new JwtSecurityTokenHandler().WriteToken(token);

            return encodetoken;
        }
    }
}
