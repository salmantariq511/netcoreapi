﻿
-------------------Steps For API end point test-------------------

1. Install the required nuget packages if not exist

2. Go to appsettings.json and UserDBContext file and change the server name with your name as mentioned below

------------e.g Server=SALMAN replace with Server=Your Server Name Of SQL SERVE


3. Delete the migrations folder if exist

4. Run command add-migration InitialCreate

5. Run command update-database

6. Your database is created in SQl server after running those above commands

7. Open Postman

8. Register the user by using the api call "https://localhost:(YourPortNumber)/api/User/register" and select option "POST"

9. Add the following Json in Body of postman to resigter the user mentioned below

-------------{  "UserName":"admin", "Password":"admin"}-----------



10. Login with credentials with postman request API "https://localhost:(YourPortNumber)/api/User/LoginUser" and select option "POST"

11. Add the following Json in Body of postman to resigter the user mentioned below

-------------{  "UserName":"admin", "Password":"admin"}-----------

12. The jwt token is returned in the response if the credentials are correct

13. Get the user list by using the api call "https://localhost:(YourPortNumber)/api/User/GetAllUser" and select option "Get"

14. Go to Authorization tab in postman and select BearerToken under TYPE dropdown

15. Paste the token which you received through Login API call by postman.

16. The list of users will be returned as Json List

